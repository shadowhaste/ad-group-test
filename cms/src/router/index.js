import Vue from "vue";
import VueRouter from "vue-router"; 
import FullPageLayout from "@/layout/FullPageLayout.vue";  
import NotFound from "@/views/NotFoundPage.vue";

Vue.use(VueRouter);


export default new VueRouter({
  mode: "history",  
  linkActiveClass: "active",
  routes: [  
    {
      path: "/",
      redirect: "/index",
      component: FullPageLayout,  
      children: [ 
        {
          path: "login",
          name: "login", 
          component: () => import("@/views/Login.vue"),
          meta: {
              requireAuth: false
          }, 
        }, 
        {
          path: "index",
          name: "index",
          component: () => import("@/views/Index.vue"),
          meta: {
              requireAuth: true
          }, 
        },   
        {
          path: "audit-log",
          name: "audit-log",
          component: () => import("@/views/Audit.vue"),
          meta: {
              requireAuth: true
          }, 
        },  
      ]
    }, 

    { path: "*", component: NotFound } 
  ],

});