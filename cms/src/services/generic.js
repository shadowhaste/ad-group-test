import api from "./api";
import auth from "./auth";

export default {    

    postRequest(endpoint, data){  
        return new Promise((resolve, reject) => {
            api.post(
                endpoint, 
                data,
                auth.getHeader()
            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    }, 

    getRequest(endpoint){  
        return new Promise((resolve, reject) => {
            api.get(
                endpoint,  
                auth.getHeader() 
            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },
    
    postRequestWithoutHeader(endpoint, data){   
        return new Promise((resolve, reject) => {
            api.post(
                endpoint, 
                data
            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },
}