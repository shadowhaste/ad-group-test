import Vue from 'vue'
import App from './App.vue'

import router from "./router/index";
import store from "./store";
import BootstrapVue from "bootstrap-vue";    
import NobleUI from "./plugins/NobleUI";    
import auth from "./services/auth";
  
Vue.use(NobleUI);
Vue.use(BootstrapVue);     

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requireAuth)) {
        if (!auth.getUser()) {
            next({
                path: "/login"
            });
        } else {
            next();
        }
    } else {
        if (!auth.getUser()){ 
            next(); 
        }  else{  
            next({
                path: "/index"
            });
        }   
    }
});

/* eslint-disable no-new */  
new Vue({
router,
store,
render: h => h(App)
}).$mount("#app");
