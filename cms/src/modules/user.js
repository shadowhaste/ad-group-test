import notificationService from "../services/notification";  
import genericService from "../services/generic";
import auth from "../services/auth";
import router from "@/router/index";

export default {
    namespaced: true,
    state: { 
        loading: false,
        errors: null,  
        data: null,   
        user: null,

        subscriptionData: null,   
        dashboardData: null,
    },

    mutations: {
        loading(state, value) {
            state.loading = value;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 
 

        setData(state, data){
            state.data = data;  
        },  

        logout(state) { 
            auth.logout();

            state.user = null;
            state.token = null;
            state.login = null;
            state.loading = false;
             
            router.push("/login");  
        }, 

    },
    
    actions:{   
        login: async ({ commit, state }) => {          
            try {   
                let response = await genericService.postRequestWithoutHeader("login", state.data);   
                auth.setUser(response.user);
                auth.setToken(response.token); 
                router.push("/");
                
            } catch (errors) {  

                if(errors.status == 422){
                    notificationService.errorModal("Incomplete information, Review your answers then try to save again. Thanks");  
                } 

                if(errors.status == 400){
                    notificationService.errorModal(errors.errors.message);
                }  
                
                if(errors.status == 404){
                    notificationService.errorModal(errors.errors.message);
                }  
 
                commit("errors", errors);
            }
        }, 
    }
}