# AD Group test (API) 
#### Prepared By: Aries Jay Traqueña
#

The AD Group test API is written in the Laravel PHP Framework.
See laravel.com/docs for the Laravel documentation.

## Requirements

- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## How to start the application

### 1. Navigate to api folder. Copy the .env.example and create a .env file at the root of the project.

### 2. Start the Docker environment.

```bash
docker-compose build
docker-compose up -d
```

### 3. Enter the docker container
Enter into the api docker container
```bash
docker exec -it ad_api bash
```

### 4. Update dependencies
From within the container
```bash
composer update
```

### 5. Run database migrations. 
From within the container run the migrations
```bash
php artisan migrate
``` 

### 5. Run database seeder. 
From within the container run the migrations
```bash
php artisan db:seed
``` 

## Running unit test
```bash
./bin/phpunit.sh
``` 

# 
# AD Group test (FRONTEND)

The AD Group test FRONTEND is written in the Vue JS Framework.

## Requirements

- [Node JS](https://nodejs.org/en/download/) 

## How to start the application

### 1. Open another terminal and navigate to cms folder. Copy the .env.example and create a .env file at the root of the project.

### 2. Start the app by these commands:

```bash
npm install
npm run serve
``` 

### 3. Open your browser and navigate to the URL below:

```bash
http://localhost:8080
``` 


## Test user

| Username          | Password     
| ----------------- | ------------- 
| johndoe123 | password123 
| monroe123 | password123 
| henry123 | password123 