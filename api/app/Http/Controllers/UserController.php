<?php


namespace App\Http\Controllers;


use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function login(Request $request) {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $login = $this->service->login($request->username, $request->password);
        if ($login->status == 200) {
            return response()->json([
                "message" => null,
                "user" => $login->user,
                "token" => $login->token,
                "refresh_token" => $login->refresh_token,
            ]);
        }

        return response()->json([
            "message" => $login->message,
            "user" => null,
            "token" => null,
            "refresh_token" => null,
        ], $login->status);
    }
}