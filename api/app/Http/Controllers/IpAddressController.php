<?php


namespace App\Http\Controllers;


use App\Services\IpAddressService;
use Illuminate\Http\Request;

class IpAddressController extends Controller
{
    public function __construct(IpAddressService $service)
    {
        $this->service = $service;
    }

    public function index(){
        $result = $this->service->findAll();
        return response()->json([
            "message" => $result->message,
            "list" => $result->model,

        ], $result->status);
    }

    public function create(Request $request){

        $this->validate($request, [
            'ip' => 'required|ipv4',
            'label' => 'required',
        ]);

        $result = $this->service->create($request->all());

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'label' => 'required',
        ]);
        $ipAddress = $this->service->find('id', $id);
        $result = $this->service->update($ipAddress->model, [ 'label' => $request->label ]);

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);
    }

    public function view($id){
        $result = $this->service->find("id", $id);
        return response()->json([
            "message" => $result->message,
            "model" => $result->model,

        ], $result->status);
    }
}