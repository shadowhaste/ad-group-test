<?php


namespace App\Http\Controllers;


use App\Services\LogService;

class AuditLogController extends Controller
{
    public function __construct(LogService $service)
    {
        $this->service = $service;
    }

    public function index(){
        $result = $this->service->findAll();
        return response()->json([
            "message" => $result->message,
            "list" => $result->model,

        ], $result->status);
    }
}