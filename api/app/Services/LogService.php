<?php


namespace App\Services;


use App\Repositories\LogRepository;
use Illuminate\Http\Request;

class LogService extends Service
{
    public function __construct(Request $request, LogRepository $repository)
    {
        parent::__construct($request, $repository);
    }
}