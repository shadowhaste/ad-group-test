<?php


namespace App\Services;


use App\Repositories\UserRepository;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService extends Service
{
    public function __construct(Request $request, UserRepository $repository)
    {
        parent::__construct($request, $repository);
    }

    public function login($username, $password) {
        $user = $this->repository->find('username', $username);
        if (!$user) {
            $this->addLog([
                'activity' => 'Login',
            ], 0);
            return (object) [
                "status" => 404,
                "message" => __("messages.user.login.404"),
                "token" => null,
                "user" => null,
            ];
        }

        if (Hash::check($password, $user->password)) {
            $this->addLog([
                'user_id' => $user->id,
                'old_data' => $user->username,
                'activity' => 'Login',
                'status' => 'success'
            ]);
            return (object) [
                "status" => 200,
                "message" => null,
                "token" => $this->generateJWTToken($user),
                "user" => $user,
                "refresh_token" => $user->generateRefreshToken(),
            ];
        }

        return (object) [
            "status" => 401,
            "message" => __("messages.user.login.401"),
            "token" => null,
            "user" => null,
            "refresh_token" => null,
        ];
    }

    protected function generateJWTToken($user) {
        $payload = [
            'iss' => 'user', // Issuer of the token
            'sub' => $user->uuid, // Subject of the token
            'iat' => Carbon::now()->timestamp, // Time when JWT was issued.
            'exp' => Carbon::now()->addHours(1)->timestamp, // Expiration time
            'user' => $user, // User
        ];
        return JWT::encode($payload, config("project.private-key.user"), 'RS256');
    }
}