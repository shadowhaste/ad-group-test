<?php


namespace App\Repositories;


use App\Models\Log;

class LogRepository extends Repository
{
    public function __construct(Log $model)
    {
        parent::__construct($model);
    }

    public function findAll() {
        return $this->model->with(['user', 'ip_address'])->orderBy('created_at', 'desc')->get();
    }
}