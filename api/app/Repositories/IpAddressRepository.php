<?php


namespace App\Repositories;


use App\Models\IpAddress;

class IpAddressRepository extends Repository
{
    public function __construct(IpAddress $model)
    {
        parent::__construct($model);
    }

    public function find($field, $id)
    {
        return $this->model->where($field, $id)->with('logs')->first();
    }

    public function findAll() {
        return $this->model->with([
            'logs' => function($query){
            $query->orderBy('created_at', 'desc');
            }
        ])
            ->orderBy('created_at', 'desc')
            ->get();
    }
}