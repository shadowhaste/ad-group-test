<?php


namespace App\Models;


class IpAddress extends Model
{
    public $table = "ip_address";

    protected $fillable = [
        'id',
        'ip',
        'label',
    ];

    public function logs(){
        return $this->hasMany(Log::class);
    }

}