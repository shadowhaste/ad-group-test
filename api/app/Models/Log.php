<?php


namespace App\Models;


class Log extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'old_data',
        'new_data',
        'activity',
        'status',
        'ip_address_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function ip_address(){
        return $this->belongsTo(IpAddress::class, 'ip_address_id', 'id');
    }
}