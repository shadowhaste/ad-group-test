<?php

/**
 * Class Helper | app/Helpers/Helper.php
 *
 * @package     Points
 * @subpackage  Points
 * @author      Aries Jay C. Traquena <atraquena@gyondu.com>
 * @version     v.1.0 (08/16/2018)
 * @copyright   Copyright (c) 2018, Traqy
 */

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class Helper
 *
 * Achievement Unlock Helper
 */
class Helper {
    public static function generateAutoTransactionNo() {
        $date = Carbon::now()->format("Ymd");
        $digits = $date . mt_rand();
        if (strlen($digits) < 50) {
            return intval($digits);
        }
        self::generateAutoTransactionNo();
    }

    public static function generateInsuranceReferenceId($value){
        $value = str_pad($value, 6, '0', STR_PAD_LEFT);
        return "IN-CL-{$value}";
    }

    public static function toCurrency($value){
        $value = number_format($value, 2);
        return "P {$value}";
    }

    public static function toDecimal($value){
        return number_format($value, 2, '.', '');
    }

    public static function enableExportDisk(){
        try{
            $target = storage_path('exports');
            $link = base_path(). '/public/exports';

            symlink($target, $link);
            shell_exec("chown -R 1000:1000 {$link}");
            shell_exec("chmod 777 -R {$link}");
        }catch(\Exception $e){
            Log::Debug('Exception', [$e->getMessage()]);
        }
    }

    public static function download($data, $filename, $disk){
        Excel::store($data, $filename, $disk);
        return Storage::disk($disk)->url($filename);
    }

    public static function formatDate($value, $format='Y-m-d'){
        try{
            return Carbon::parse($value)->format($format);
        }catch (\Exception $e){
            Log::Debug("Error", [$e->getMessage()]);
        }
        return '';
    }

}
