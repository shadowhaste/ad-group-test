<?php


namespace App\Traits;


use App\Repositories\LogRepository;

trait LogTrait
{
    public function addLog($data, $status = 1){
        $data['status'] = ($status < 1) ? 'failed' : 'success';
        $logRepository = app(LogRepository::class);
        $logRepository->create($data);
    }
}