<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(\App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->uuid,
        'username' => $faker->userName,
        'password' => 'password123',
    ];
});

$factory->define(\App\Models\IpAddress::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->uuid,
        'ip' => long2ip(mt_rand()),
        'label' => $faker->word,
    ];
});