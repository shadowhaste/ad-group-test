<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    protected $service;
    public function __construct(\App\Services\UserService $service)
    {
        $this->service = $service;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->service->create([
            "username" => "johndoe123",
            "password" => "password123",
        ]);

        $this->service->create([
            "username" => "monroe123",
            "password" => "password123",
        ]);

        $this->service->create([
            "username" => "henry123",
            "password" => "password123",
        ]);
    }
}
