<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post("login", "UserController@login");
$router->group(['middleware' => "auth:user", "prefix" => "ip-address"], function() use ($router) {
    $router->post("/", "IpAddressController@create");
    $router->get("/", "IpAddressController@index");
    $router->post("/{id}", "IpAddressController@update");
    $router->get("/{id}", "IpAddressController@view");
});

$router->group(['middleware' => "auth:user", "prefix" => "logs"], function() use ($router) { ;
    $router->get("/", "AuditLogController@index");
});