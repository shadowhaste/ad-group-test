<?php

return [
    'user' => [
        'login' => [
            '404' => 'User does not exist.'
        ]
    ],
    'ip_address' => [
        'create' => [
            '200' => 'IP address created.'
        ],
        'update' => [
            '200' => 'IP address updated.'
        ],
    ]
];
