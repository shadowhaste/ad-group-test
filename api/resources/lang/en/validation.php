<?php

return [
    'required' => 'The :attribute is required.',
    'unique' => 'The :attribute already exist.',
    'ipv4' => 'Invalid IP address.'
];
