<?php


class AppTest extends TestCase
{
    public function testLogin() {
        //test 422
        $response = $this->post("/login", []);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'username',
            'password'
        ]);

        //test 200
        $response = $this->post("/login", [
            'username' => 'johndoe123',
            'password' => 'password123'
        ]);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'user'
        ]);
    }

    public function testAddIp() {
        $user = factory(\App\Models\User::class)->create();
        $response = $this->post("/ip-address", [], $this->UserHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'ip',
            'label',
        ]);

        $testdata = factory(\App\Models\IpAddress::class)->make();
        $response = $this->post("/ip-address", [
            'ip' => str_random(10),
            'label' => $testdata->label,
        ], $this->UserHeader($user));
        $response->assertResponseStatus(422);

        $testdata = factory(\App\Models\IpAddress::class)->make();
        $response = $this->post("/ip-address", [
            'ip' => $testdata->ip,
            'label' => $testdata->label,
        ], $this->UserHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testUpdateIp(){
        $user = factory(\App\Models\User::class)->create();

        $createdData = factory(\App\Models\IpAddress::class)->create();

        $response = $this->post("/ip-address/{$createdData->id}", [], $this->UserHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'label',
        ]);

        $updateData = factory(\App\Models\IpAddress::class)->make();
        $response = $this->post("/ip-address/{$createdData->id}", [
            'label' => $updateData->label,
        ], $this->UserHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testViewIp(){
        $user = factory(\App\Models\User::class)->create();

        //create
        $testdata = factory(\App\Models\IpAddress::class)->make();
        $response = $this->post("/ip-address", [
            'ip' => $testdata->ip,
            'label' => $testdata->label,
        ], $this->UserHeader($user));

        $data = $this->decode($response);
        $ipAddressId = $data->model->id;
        $response = $this->get("/ip-address/{$ipAddressId}", $this->UserHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testIpAddressList(){
        $user = factory(\App\Models\User::class)->create();

        for ($i=0; $i<10; $i++){
            $testdata = factory(\App\Models\IpAddress::class)->make();
            $response = $this->post("/ip-address", [
                'ip' => $testdata->ip,
                'label' => $testdata->label,
            ], $this->UserHeader($user));
        }

        $response = $this->get("/ip-address", $this->UserHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testGetLogs(){
        $user = factory(\App\Models\User::class)->create();

        for ($i=0; $i<10; $i++){
            $testdata = factory(\App\Models\IpAddress::class)->make();
            $response = $this->post("/ip-address", [
                'ip' => $testdata->ip,
                'label' => $testdata->label,
            ], $this->UserHeader($user));
        }

        $response = $this->get("/logs", $this->UserHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }
}